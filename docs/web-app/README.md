# Webseite statt App

Wir haben uns entschieden, eine Website statt einer App zu erstellen.

Eine Installation ist nicht erforderlich, und die Nutzer:innen müssen keine persönlichen Daten in App-Stores angeben.

## Datenschutz 

Auf die Website kann z.B. über den Tor-Browser zugegriffen werden, und das Gerät kann für den Nutzungszeitraum vom Internet getrennt werden. (Bilder wären dann allerdings nicht zu sehen).

Es werden keine Daten bei der App Nutzung zum Server gesendet oder gespeichert.
