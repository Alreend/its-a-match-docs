# Inhaltsverwaltung

## Erklärvideo

<VideoPlayer src="/videos/inhaltsverwaltung.mp4" :chapters="{ 16:'Kategorie Fragen', 107:'Kategorie Jobs', 357:'Kategorie Gruppen', 374:'Kategorie Ortsgruppen', 425:'Job einpflegen', 436:'Matching konfigurieren', 487:'Daten validieren', 594:'Matching anpassen', 650:'Relevanz nachjustieren', 679:'Fehler melden', }" />

## Jobs einreichen

Wir brauchen Jobs, die als Ergebnisse angezeigt werden können.

### Welche Infos brauchen wir pro Job?

Alle Infos sind optional, aber je mehr wir haben, desto besser.

- **Job-Titel**: Der Titel des Jobs, z.B. "Aktions Sanis"
- **Job-Beschreibung**: Eine kurze Beschreibung des Jobs, z.B. "Auf Demos mitgehen und verletzte Personen mit erster
  Hilfe versorgen."
- **Job-Foto**: Ein Foto des Jobs, z.B. ein Bild von einem Sani, gerne im Querformat und nicht größer als 1 MB. Das
  untere drittel des Bildes wird mit dem Job-Titel und der Job-Beschreibung überlagert.
- **Job-Kompetenzen**: Die Kompetenzen, die der Job erfordert, z.B. "Stress aushalten, ruhig bleiben im Notfall, Erste
  Hilfe können, Erste Hilfe lernen wollen, Polizeikontakt (wenig-viel)".
- **Job-Datum**: Falls der Job nur an einem Datum stattfinden soll, dann das z.B. "22. Januar 2020"  oder ein
  Datumsbereich, z.B. "22. Januar 2020 - 24. Januar 2020"
- **Job-Ort**: Der Ort, an dem der Job stattfinden soll, z.B. "Berlin"
- **Job-Bewerbung**: Die Mailadresse, an die sich interesierte wenden können, z.B. "sanis@fff.de"

## Jobs hinzufügen

Die genannten Jobs selbst in die Datenbank einfügen.

### Login

Einloggen unter [https://klimatinder.sanity.studio/](https://klimatinder.sanity.studio/).
Zugangsdaten bekommt ihr von uns in Kombination mit einer kleinen technischen Einweisung.

### Übersicht

Auf der Seite gibt es vier Kategorien.

- Fragen: Fragen, die zur Berechnung der Job-Matches dienen.
- Jobs: Enthalten Kontaktinformationen, Tätigkeitsbeschreibungen und Berechnungsvorgaben.
- Gruppen: Gruppen wie z.B. FFF, XR, EG mit den jeweiligen Kontakt-Emails
- Ortsgruppen: Ortsgruppen mit eigenen Emails

::: warning Fragen und Gruppen bitte nur in Rücksprache bearbeiten/hinzufügen
Da die Fragen und Gruppen sehr direkt in den Algorithmus eingebunden sind, sollten sie nur in Rücksprache hinzugefügt
oder bearbeitet werden.

Fügt gerne eigene Jobs hinzu und bearbeitet auch gerne bestehende Jobs, und Ortsgruppen.
:::

### Einpflegen

Hinzufügen unter [https://klimatinder.sanity.studio/desk/job](https://klimatinder.sanity.studio/desk/job).
Am Ende auf "Publish" drücken.

### Matching Algorithmus

In den folgenden Beispielen wird die Relevanz eines Jobs in Abhhängikeit von gegebenen Antworten berechnet.

| Antworttext | Korrespondierende Zahl | 
|-------------|------------------------|
| Nein        | -1                     |
| Neutral     | 0                      |
| Ja          | 1                      |
| Ja!         | 2                      |

Der Score gibt an, wieviele der Bedingungen erfüllt wurden.
Dazu werden alle Relevanzen der erfüllten Regeln addiert, und nicht erfüllte Regeln subtrahiert.

::: tip Besondere Relevanz
Bei mit "Ja!" beantworteten Fragen oder nicht erfüllten Fragen wird die Relevanz mit 1.2 exponentiert, um eine größere
Auswirkung zu erhalten.
:::

#### Beispiel 1

| Relevanz | Frage                              | Erforderliche Antwort | Gegebene Antwort                      | Berechnung |
|----------|------------------------------------|-----------------------|---------------------------------------|------------|
| 50       | Ich kann vor Menschen sprechen.    | ‹0, 1, 2›                | 0   <br/>   (zutreffend=>1.0)         | +50 ^ 1.0  |
| 60       | Ich kann nur von zuhause arbeiten     | ‹-1, 1, 2›            | 0    <br/>    (nicht zutreffend=>1.2) | -60 ^ 1.2  |                  

Berechnung: `50^1.0 - 60^1.2 = -77` und daraus wird `-77 / (50+60) = -64%`

#### Beispiel 2

| Relevanz | Frage                              | Erforderliche Antwort | Gegebene Antwort                                                | Berechnung                                                                 |
|----------|------------------------------------|-----------------------|-----------------------------------------------------------------|----------------------------------------------------------------------------|
| 50       | Ich kann vor Menschen sprechen.    | ‹0, 1, 2›                | 0   <br/>   (zutreffend=>1.0)                                   | +50 ^ 1.0                                                                  |
| 60       | Ich kann nur von zuhause arbeiten     | ‹-1, 1, 2›            | <span style=color:orange>1    <br/>    (zutreffend=>1.0)</span> | <span style=color:orange>+</span>60 ^  <span style=color:orange>1.0</span> |                  

Berechnung: `50^1.0 + 60^1.0 = 110` und daraus wird `110 / (50+60) = 100%`

#### Beispiel 3

| Relevanz | Frage                              | Erforderliche Antwort | Gegebene Antwort                                                                       | Berechnung                             |
|----------|------------------------------------|-----------------------|----------------------------------------------------------------------------------------|----------------------------------------|
| 50       | Ich kann vor Menschen sprechen.    | ‹0, 1, 2›                | <span style=color:red>2</span>  <br/>   (zutreffend=><span style=color:red>1.2</span>) | +50 ^ <span style=color:red>1.2</span> |
| 60       | Ich kann nur von zuhause arbeiten     | ‹-1, 1, 2›            | 0    <br/>    (nicht zutreffend=>1.2)                                                  | -60 ^ 1.2                              |                  

Berechnung: `50^1.2 - 60^1.2 = -27` und daraus wird `-27 / (50+60) = -24%`

### Daten validieren

Besucht das Entwicklungstool
unter [https://klimafinder.netlify.app/finder/debug](https://klimafinder.netlify.app/finder/debug).

::: danger Bekannte Probleme in Safari
:warning: Bitte benutze Firefox, Chrome oder Edge, da Safari Probleme mit dem CORS-Header hat.
:::

1. Fragen so anklicken, dass der Job angezeigt werden müsste.
    - Wird der Job oben angezeigt? (Nötigenfalls Relevanzen anpassen)
2. Fragen so anklicken, dass der Job nicht angezeigt wird.
    - Wird der Job oben nicht angezeigt? (Nötigenfalls Relevanzen anpassen)
3. Gibt es Kombinationen von Fragen und Jobs, die problematisch sind?
    - Beispielsweise "Ich kann nur von zuhause arbeiten" wird mit Ja beantwortet, aber der Job "Massenaktion" wird
      angezeigt, weil der Rest zutrifft.
    - Nötigenfalls Entscheidungen, Antworten und Relevanzen anpassen.
    - Ggf. in Rücksprache Fragen bearbeiten/hinzufügen.

::: tip Relevanzen anpassen
Jetzt wollen wir einmal die Relevanz des Jobs anpassen, um bei einem Ja! den Skillshare anzuzeigen, auch wenn die Person
die zweite Frage nicht zutreffend angegeben hat.
:::

#### Beispiel 3 (angepasst)

Job-Titel: "Skillshare"

| Relevanz                        | Frage                              | Erforderliche Antwort | Gegebene Antwort                      | Berechnung                            |
|---------------------------------|------------------------------------|-----------------------|---------------------------------------|---------------------------------------|
| <span style=color:red>70</span> | Ich kann vor Menschen sprechen.    | ‹0, 1, 2›                | 2 <br/>   (zutreffend=>1.2)           | +<span style=color:red>70</span> ^1.2 |
| 60                              | Ich kann nur von zuhause arbeiten     | ‹-1, 1, 2›            | 0    <br/>    (nicht zutreffend=>1.2) | -60 ^ 1.2                             |                  

Berechnung: `70^1.2 - 60^1.2 = 27` und daraus wird `27 / (50+60) = 24%`

### Notwendige Kriterien

Es kann Kriterien geben, die unbedingt erfüllt sein müssen, damit ein Job angezeigt wird.
Dazu gibt es im Backend eine Checkbox "Notwendig", die bei diesen Kriterien gesetzt werden kann.

#### Beispiel 1

| Relevanz                                   | Frage                             | Erforderliche Antwort | Gegebene Antwort                      | Berechnung                                     |
|--------------------------------------------|-----------------------------------|-----------------------|---------------------------------------|------------------------------------------------|
| 50                                         | Ich kann vor Menschen sprechen.   | ‹0, 1, 2›             | 2  <br/>   (zutreffend=>1.2)          | +50 ^ 1.2                                      |
| 60, <span style=color:red>Notwendig</span> | Ich kann nur von zuhause arbeiten | ‹-1, 1, 2›            | 0    <br/>    (nicht zutreffend=>1.2) | -60 ^ 1.2  <span style=color:red>-10000</span> |                  

Berechnung: `50^1.2 - 60^1.2 - 10000 = -10027` und daraus wird `-10027 / (50+60) = -9115%`

Dieser Job würde also niemals angezeigt werden, weil die Relevanz immer negativ ist, solange es keine Übereinstimmung
gibt.

#### Beispiel 2

| Relevanz                                   | Frage                             | Erforderliche Antwort | Gegebene Antwort                                               | Berechnung |
|--------------------------------------------|-----------------------------------|-----------------------|----------------------------------------------------------------|------------|
| 50                                         | Ich kann vor Menschen sprechen.   | ‹0, 1, 2›             | 2  <br/>   (zutreffend=>1.2)                                   | +50 ^ 1.2  |
| 60, <span style=color:red>Notwendig</span> | Ich kann nur von zuhause arbeiten | ‹-1, 1, 2›            | <span style=color:red>1    <br/>    (zutreffend=>1)    </span> | -60 ^ 1.0  |                  

Berechnung: `50^1.2 - 60^1.0 = 49` und daraus wird `49 / (50+60) = 40%`

Wenn die zweite Frage zutrifft, wird der Job ganz normal angezeigt.

## Schnelleres Arbeiten

::: tip Live-Aktualisierung
Nutze den "Refresh data" Button im Entwicklungstool, um Änderungen in Sanity direkt in Klima Finder zu aktualisieren.
Auch das ändern von Antworten ist im Entwicklungstool über die Dropdown Buttons möglich.
:::

## Fehler gefunden?

Bitte eine Email damit senden an folgende Adresse:

<incoming+alreend-klimatinder-37332484-el649dsb7oqo7e7aildbpflkf-issue@incoming.gitlab.com>