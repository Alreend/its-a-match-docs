const {defaultTheme} = require('@vuepress/theme-default')
const {registerComponentsPlugin} = require('@vuepress/plugin-register-components')
const { path } = require('@vuepress/utils')


module.exports = {
    lang: 'en-US',
    title: 'Klima Finder',
    description: 'Find your climate movement',
    tip: 'Developers',
    theme: defaultTheme({
        logo: '/images/icon.png',
        darkMode: true,
        navbar: [
            // NavbarItem
            '/about/',
            '/contact/',
            {text: 'Zum Protoyp', link: 'https://klimafinder.netlify.app/'},
        ],
        sidebar: ['/about/', '/web-app/', '/join/', '/contact']
    }),
    plugins: [
        registerComponentsPlugin({
            components: {
                VideoPlayer:  path.resolve(__dirname, './components/VideoPlayer.vue'),
            }
        })
    ]
}
